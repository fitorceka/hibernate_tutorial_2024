package exercises;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.util.List;
import java.util.Optional;

public class GenreRepository {

    private SessionFactory sessionFactory;

    public GenreRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    public void save(Genre genre) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.persist(genre);
            tx.commit();
        }
    }

    public void delete(Genre genre) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            session.remove(genre);
            tx.commit();
        }
    }

    public Optional<Genre> findByName(String name) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            Optional<Genre> found = Optional.of(session.createQuery("select G from Genre G " +
                    "where G.name = :nam", Genre.class)
                    .setParameter("nam", name)
                    .getSingleResult());
            tx.commit();
            return found;
        }
    }

    public Optional<Genre> findById(int id) {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            Optional<Genre> found = Optional.of(
                    session.find(Genre.class, id));
            tx.commit();
            return found;
        }
    }

    public List<Genre> findAll() {
        Transaction tx = null;
        try (Session session = sessionFactory.openSession()) {
            tx = session.beginTransaction();
            List<Genre> found = session.createQuery("select G from Genre G",
                            Genre.class)
                    .getResultList();
            tx.commit();
            return found;
        }
    }
}
