package exercises;

import entity.Base;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToMany;
import jakarta.persistence.ManyToOne;

import java.util.List;

@Entity
public class Movie extends Base {

    @Column(name = "TITLE")
    private String title;

    @Column(name = "YEAR_OF_RELEASE")
    private int yearOfRelease;

    @ManyToMany(mappedBy = "movies")
    private List<Actor> actors;

    @ManyToOne
    @JoinColumn(name = "GENRE_ID")
    private Genre genre;

    public Movie(String title, int yearOfRelease, List<Actor> actors, Genre genre) {
        this.title = title;
        this.yearOfRelease = yearOfRelease;
        this.actors = actors;
        this.genre = genre;
    }

    public Movie() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }
}
