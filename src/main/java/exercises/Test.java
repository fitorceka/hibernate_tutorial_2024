package exercises;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.List;
import java.util.Optional;
import java.util.Scanner;

public class Test {

    public static void main(String[] args) {
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        GenreRepository genreRepository = new GenreRepository(sessionFactory);
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Welcome to the system!");
            System.out.println("Press quit to exit: ");
            String exit = scanner.nextLine();

            if (exit.equals("quit")) {
                System.out.println("Logout from system!");
                break;
            }

            System.out.println("Pres 1 to add a Genre in the system: ");
            System.out.println("Pres 2 to delete a Genre in the system: ");
            System.out.println("Pres 3 to find a Genre by name: ");
            System.out.println("Pres 4 to find all: ");

            int press = scanner.nextInt();
            scanner.close();
            if (press == 1) {
                System.out.println("Enter Genre name: ");
                scanner = new Scanner(System.in);
                String name = scanner.next();
                Genre genre = new Genre(name);
                System.out.println("Saving Genre .....");
                genreRepository.save(genre);
            }
            else if (press == 2) {
                System.out.println("Enter the Genre id: ");
                int id = scanner.nextInt();
                Optional<Genre> found = genreRepository.findById(id);
                if (found.isPresent()) {
                    System.out.println("Deleting ....");
                    genreRepository.delete(found.get());
                }
                else {
                    System.out.println("Cannot delete!");
                }
            }
            else if (press == 3) {
                System.out.println("Enter the Genre name: ");
                String name = scanner.nextLine();
                Optional<Genre> found = genreRepository.findByName(name);
                if (found.isPresent()) {
                    System.out.println("Found ....");
                    System.out.println(found);
                }
                else {
                    System.out.println("Cannot find Genre by name " + name);
                }
            }
            else if (press == 4) {
                List<Genre> genres = genreRepository.findAll();
                System.out.println("Found " + genres.size() + " records in the DB.");
                genres.forEach(System.out::println);
            }
            else {
                System.out.println("Invalid operation!!!");
            }
        }
    }
}
