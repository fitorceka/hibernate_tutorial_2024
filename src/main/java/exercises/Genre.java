package exercises;

import entity.Base;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;

@Entity
public class Genre extends Base {

    @Column(name = "NAME")
    private String name;

    public Genre(String name) {
        this.name = name;
    }

    public Genre() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Genre{" +
                "name='" + name + '\'' +
                '}';
    }
}
