package test;

import entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import java.time.LocalDate;

public class Test {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

//        Student student = new Student("Filan", "Fistek", LocalDate.now());
//        Address address = new Address();
//        address.setStreet("Rruga");
//        address.setBuilding("Edith Durham");
//        address.setZipCode(100);
//        student.setAddress(address);
//
//        //per te ruajtur te dhena ne data baze
////        session.persist(student);
//
//        // per te nxjerre te dhena nga databaza
//        System.out.println("Before update");
//        Student found = session.find(Student.class, 1);
//        System.out.println(found);
//
//        //per te updateuar te dhena nga databaza
//        System.out.println("---------------------------------------");
//        System.out.println("after update");
//        found.setBirthDay(LocalDate.of(1999, Month.MAY, 20));
//        session.merge(found);
//        found = session.find(Student.class, 1);
//        System.out.println(found);
//
//        // per te fshire
//        session.remove(found);
//
//        found = session.find(Student.class, 1);
//        System.out.println("After Delete");
//        System.out.println(found);


        // u perdor per lidhjen 1 me 1
//        Student student = new Student("Filan", "Fistek", LocalDate.now());
//        Address address = new Address();
//        address.setStreet("Rruga");
//        address.setBuilding("Edith Durham");
//        address.setZipCode(100);
//        student.setAddress(address);
//
//        StudentExtra se = new StudentExtra("12233", "hgcg@gma", true);
//        student.setStudentExtra(se);
//
//        session.persist(student);
//
//        Student found = session.find(Student.class, 2);
//        session.remove(found);

        // this was one to many relationship
//        Student student = new Student("Filan", "Fistek", LocalDate.now());
//        Address address = new Address();
//        address.setStreet("Rruga");
//        address.setBuilding("Edith Durham");
//        address.setZipCode(100);
//        student.setAddress(address);
//        Student student1 = new Student("Filan1", "Fistek1", LocalDate.now());
//        student1.setAddress(address);
//
//        School school = new School("Edith", address, new ArrayList<>());
//
//        student.setSchool(school);
//        student1.setSchool(school);
//
//        session.persist(school);
//        session.persist(student);
//        session.persist(student1);

//        School found = session.find(School.class, 1);
//
//        System.out.println(found);

        // for Many to Many relationship

//        Student s1 = new Student("Filan1", "Fistek1", LocalDate.now());
//        Address address = new Address();
//        address.setStreet("Rruga");
//        address.setBuilding("Edith Durham");
//        address.setZipCode(100);
//        s1.setAddress(address);
//        Student s2 = new Student("Filan2", "Fistek2", LocalDate.now());
//        s2.setAddress(address);
//        Student s3 = new Student("Filan3", "Fistek3", LocalDate.now());
//        s3.setAddress(address);
//
//        Exam e1 = new Exam("Math", 30);
//        Exam e2 = new Exam("Biology", 20);
//        Exam e3 = new Exam("History", 50);
//
//        s1.setExams(List.of(e1, e2, e3));
//        s2.setExams(List.of(e3));
//        s3.setExams(List.of(e1, e2));
//
//        session.persist(e1);
//        session.persist(e2);
//        session.persist(e3);
//
//        session.persist(s1);
//        session.persist(s2);
//        session.persist(s3);

        Student s1 = new Student("Filan4", "Fistek4", LocalDate.of(2000, 5, 20));
        Student s2 = new Student("Filan5", "Fistek5", LocalDate.of(2001, 5, 20));
        Student s3 = new Student("Filan6", "Fistek6", LocalDate.of(2002, 7, 20));
        Student s4 = new Student("Filan7", "Fistek7", LocalDate.of(2001, 10, 20));

        session.persist(s1);
        session.persist(s2);
        session.persist(s3);
        session.persist(s4);

        transaction.commit();
    }
}
