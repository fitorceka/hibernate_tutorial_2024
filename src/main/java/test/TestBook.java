package test;

import entity.BookE;
import entity.ids.BookId;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;


public class TestBook {

    public static void main(String[] args) {
        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

        BookId id = new BookId(1, "Book1");
        BookE book = new BookE(id, "Gjenearl");

        session.persist(book);
        transaction.commit();
    }
}
