package entity.hql;

import entity.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

public class TestFindAll {

    public static void main(String[] args) {

        SessionFactory sessionFactory = new Configuration()
                .configure("hibernate.cfg.xml")
                .buildSessionFactory();

        Session session = sessionFactory.openSession();

        Transaction transaction = session.beginTransaction();

//        Query<Student> query = session.createQuery("from Student", Student.class);
//
//        List<Student> students = query.getResultList();

//        Query<String> query = session.createQuery("select S.firstName from Student S", String.class)
//                .setMaxResults(3);
//
//        List<String> names = query.getResultList();
//
//        System.out.println(names);

        // gjej studentet me parametrizim
//        Query<Student> query = session.createQuery("select S " +
//                "from Student S where S.firstName = :fn and S.address.zipCode = :zip", Student.class);
//
//        Scanner scanner = new Scanner(System.in);
//        System.out.print("Enter name to search: ");
//        String search = scanner.nextLine();
//
//        System.out.print("Enter zip: ");
//        int zip = scanner.nextInt();
//
//        query.setParameter("fn", search);
//        query.setParameter("zip", zip);

        Query<Student> hql = session.createQuery(
                "select S from Student S order by S.birthDay DESC", Student.class);

        List<Student> students = hql.getResultList();

        students.forEach(System.out::println);


        transaction.commit();
    }
}
