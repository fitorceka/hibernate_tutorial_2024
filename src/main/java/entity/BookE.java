package entity;

import entity.ids.BookId;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;

@Entity
public class BookE {

    @EmbeddedId
    private BookId id;

    private String bookName;

    public BookE(BookId id, String bookName) {
        this.id = id;
        this.bookName = bookName;
    }

    public BookE() {
    }
}
