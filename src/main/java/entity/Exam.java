package entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.ManyToMany;

import java.util.List;

@Entity
public class Exam extends Base {

    private String name;

    @Column(name = "NO_OF_QUESTIONS")
    private int noOfQuestion;

    @ManyToMany(mappedBy = "exams")
    private List<Student> students;

    public Exam(String name, int noOfQuestion) {
        this.name = name;
        this.noOfQuestion = noOfQuestion;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNoOfQuestion() {
        return noOfQuestion;
    }

    public void setNoOfQuestion(int noOfQuestion) {
        this.noOfQuestion = noOfQuestion;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
