package entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embedded;
import jakarta.persistence.Entity;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

import java.util.List;

@Entity
@Table(name = "SCHOOL_INFO")
public class School extends Base {

    @Column(name = "NAME", nullable = false)
    private String name;

    @Embedded
    private Address address;

    @OneToMany(mappedBy = "school")
    private List<Student> students;

    public School() {
    }

    public School(String name, Address address, List<Student> students) {
        this.name = name;
        this.address = address;
        this.students = students;
    }
}
