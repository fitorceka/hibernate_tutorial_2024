package entity.ids;

import jakarta.persistence.Embeddable;

import java.io.Serializable;

@Embeddable
public class BookId implements Serializable {

    private int id1;
    private String id2;

    public BookId(int id1, String id2) {
        this.id1 = id1;
        this.id2 = id2;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BookId bookId = (BookId) o;

        if (id1 != bookId.id1) return false;
        return id2.equals(bookId.id2);
    }

    @Override
    public int hashCode() {
        int result = id1;
        result = 31 * result + id2.hashCode();
        return result;
    }
}
