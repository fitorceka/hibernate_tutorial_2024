package entity;

import jakarta.persistence.Entity;
import jakarta.persistence.OneToOne;

@Entity
public class StudentExtra extends Base {

    private String number;
    private String email;
    private boolean isExcellent;

    @OneToOne(mappedBy = "studentExtra")
    private Student student;

    public StudentExtra(String number, String email, boolean isExcellent) {
        this.number = number;
        this.email = email;
        this.isExcellent = isExcellent;
    }

    public StudentExtra() {
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isExcellent() {
        return isExcellent;
    }

    public void setExcellent(boolean excellent) {
        isExcellent = excellent;
    }
}
